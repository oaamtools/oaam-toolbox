//init namespace

export function init(pluginAPI,config) {  
    //plug-in statics and configuration
    var scriptIncludes=[
    ];

    var stylesheetIncludes=[
        'css/theme.css',
        'css/contextMenu.css',
        'css/tree-workspaceSticky/ui.tree-workspaceSticky.css',
        'css/NotesTool.css',
    ];
    
    //init the plug-in
    return pluginAPI.loadScripts(scriptIncludes).then(function(){ 
        return pluginAPI.loadStylesheets(stylesheetIncludes).then(function(){ 
            // nothing to do
            return Promise.resolve(); });   
        });
};

export var meta={
    id:"oaamToolboxTheme",
    description:"Color scheme and icons for the OAAM Toolbox",
    author:"Matthias Brunner",
    version:"1.0.0",
	config : {},
    requires:['plugin.contextMenu','plugin.fancyTree','tool.notes'] //force loading of all themed plugins, so that they can be themed as well
};



