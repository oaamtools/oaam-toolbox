@echo off
@echo Preparing OAAM Toolbox virtual environment...
pip install virtualenv
python -m venv oaamtools-env
@echo Installing required packages in OAAM Toolbox virtual environment...
cmd /k ".\oaamtools-env\Scripts\activate & pip install -r requirements.txt & deactivate & exit"
@echo OAAM Toolbox is now ready to use.