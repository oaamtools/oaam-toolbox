

# OAAM Toolbox

## Overview
The OAAM Toolbox is an XGEE application for editing OAAM, featuring two OAAM-specific actions: auto-routing and Dijkstra's algorithm. This toolbox aims to facilitate the visualization and routing processes in various applications.

## Installation

### Recommended Method via PyPI
To get started with the OAAM Toolbox, first install XGEE, which is the foundation for its visualization capabilities:

```sh
pip install XGEE
```

Next, clone the OAAM Toolbox repository along with all its submodules:

```sh
git clone --recurse-submodules git@gitlab.com:oaamtools/oaam-toolbox.git
```

*Note: The OAAM routing action is not publicly available. Please request access if needed.*

*Although the `setup.bat` and `requirements.txt` methods might work, they have not been tested or updated recently.*

## Running the Application
To run the OAAM Toolbox, navigate to the cloned repository folder and execute the following command:

```sh
xgee
```

## Current Status
Please note that this project is currently without a maintainer. Contributions and maintenance support are welcome.